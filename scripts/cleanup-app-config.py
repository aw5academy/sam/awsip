import requests
import json
import os
import boto3

AWS_REGION = os.getenv('AWS_REGION')
APPCONFIG = boto3.client('appconfig', region_name=AWS_REGION)
APP_NAME = 'awsip'
ENV_NAME = 'live'
DEPLOY_STRATEGY_NAME = 'AllAtOnceNoWaitNoBake'

def cleanup():
  application_id = get_application_id()
  if application_id:
    config_profiles = APPCONFIG.list_configuration_profiles(
      ApplicationId=application_id,
    )
    for config_profile in config_profiles['Items']:
      config_versions = APPCONFIG.list_hosted_configuration_versions(
        ApplicationId=application_id,
        ConfigurationProfileId=config_profile['Id'],
      )
      for config_version in config_versions['Items']:
        response = APPCONFIG.delete_hosted_configuration_version(
          ApplicationId=application_id,
          ConfigurationProfileId=config_profile['Id'],
          VersionNumber=config_version['VersionNumber']
        )
      response = APPCONFIG.delete_configuration_profile(
        ApplicationId=application_id,
        ConfigurationProfileId=config_profile['Id']
      )
    environment_id = get_environment_id(application_id)
    if environment_id:
      response = APPCONFIG.delete_environment(
        ApplicationId=application_id,
        EnvironmentId=environment_id
      )
    response = APPCONFIG.delete_application(
      ApplicationId=application_id
    )
  deploy_strategy_id = get_deploy_strategy_id()
  if deploy_strategy_id:
    response = APPCONFIG.delete_deployment_strategy(
      DeploymentStrategyId=deploy_strategy_id
    )

def get_application_id():
  applications = APPCONFIG.list_applications()
  for application in applications['Items']:
    if application['Name'] == APP_NAME:
      return application['Id']
  return None

def get_environment_id(application_id):
  environments = APPCONFIG.list_environments(
      ApplicationId=application_id,
  )
  for environment in environments['Items']:
    if environment['Name'] == ENV_NAME:
      return environment['Id']
  return None

def get_deploy_strategy_id():
  deploy_strategies = APPCONFIG.list_deployment_strategies()
  for deploy_strategy in deploy_strategies['Items']:
    if deploy_strategy['Name'] == DEPLOY_STRATEGY_NAME:
      return deploy_strategy['Id']
  return None

cleanup()
