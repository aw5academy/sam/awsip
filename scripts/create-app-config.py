import requests
import json
import os
import boto3

AWS_REGION = os.getenv('AWS_REGION')
APPCONFIG = boto3.client('appconfig', region_name=AWS_REGION)
APP_NAME = 'awsip'
ENV_NAME = 'live'
DEPLOY_STRATEGY_NAME = 'AllAtOnceNoWaitNoBake'
MAX_IPS_PER_APP_CONFIG_PROFILE = 3000

def get_application_id():
  applications = APPCONFIG.list_applications()
  for application in applications['Items']:
    if application['Name'] == APP_NAME:
      return application['Id']
  return create_application()

def create_application():
  response = APPCONFIG.create_application(
    Name=APP_NAME,
  )
  return response['Id']

def get_config_profile_id(config_profile_index, application_id):
  config_profiles = APPCONFIG.list_configuration_profiles(
      ApplicationId=application_id,
  )
  for config_profile in config_profiles['Items']:
    if config_profile['Name'] == str(config_profile_index):
      return config_profile['Id']
  return create_config_profile(config_profile_index, application_id)

def create_config_profile(config_profile_index, application_id):
  response = APPCONFIG.create_configuration_profile(
      ApplicationId=application_id,
      Name=str(config_profile_index),
      LocationUri='hosted',
  )
  return response['Id']

def create_hosted_configuration_version(config_profile_id, ips, application_id):
  ips_string = json.dumps(ips)
  response = APPCONFIG.create_hosted_configuration_version(
    ApplicationId=application_id,
    ConfigurationProfileId=config_profile_id,
    Content=bytes(ips_string, 'utf-8'),
    ContentType='application/json',
  )
  return response['VersionNumber']

def get_environment_id(application_id):
  environments = APPCONFIG.list_environments(
      ApplicationId=application_id,
  )
  for environment in environments['Items']:
    if environment['Name'] == ENV_NAME:
      return environment['Id']
  return create_environment(application_id)

def create_environment(application_id):
  response = APPCONFIG.create_environment(
    ApplicationId=application_id,
    Name=ENV_NAME
  )
  return response['Id']

def get_deploy_strategy_id():
  deploy_strategies = APPCONFIG.list_deployment_strategies()
  for deploy_strategy in deploy_strategies['Items']:
    if deploy_strategy['Name'] == DEPLOY_STRATEGY_NAME:
      return deploy_strategy['Id']
  return create_deploy_strategy()

def create_deploy_strategy():
  response = APPCONFIG.create_deployment_strategy(
    Name=DEPLOY_STRATEGY_NAME,
    DeploymentDurationInMinutes=0,
    FinalBakeTimeInMinutes=0,
    GrowthFactor=100.0,
    GrowthType='LINEAR',
    ReplicateTo='NONE'
  )
  return response['Id']

def start_deployment(config_profile_id, config_version_id, application_id, environment_id, deploy_strategy_id):
  response = APPCONFIG.start_deployment(
      ApplicationId=application_id,
      EnvironmentId=environment_id,
      DeploymentStrategyId=deploy_strategy_id,
      ConfigurationProfileId=config_profile_id,
      ConfigurationVersion=config_version_id,
  )

def ips_to_app_config():
  ip_ranges = requests.get('https://ip-ranges.amazonaws.com/ip-ranges.json').json()['prefixes']
  ips=[]
  config_profile_index = 0
  item_index = 0
  for item in ip_ranges:
    item_index = item_index + 1
    ips.append(item['ip_prefix'])
    if len(ips) == MAX_IPS_PER_APP_CONFIG_PROFILE or item_index == len(ip_ranges):
      config_profile_index = config_profile_index + 1
      application_id = get_application_id()
      config_profile_id = get_config_profile_id(config_profile_index, application_id)
      config_version_id = create_hosted_configuration_version(config_profile_id, ips, application_id)
      environment_id = get_environment_id(application_id)
      deploy_strategy_id = get_deploy_strategy_id()
      start_deployment(config_profile_id, config_version_id, application_id, environment_id, deploy_strategy_id)
      ips.clear()

ips_to_app_config()
