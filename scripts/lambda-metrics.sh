#!/bin/bash

FUNCTION_NAME="$1"

duration_sum=0.0
for i in {1..20}; do
  duration=$(aws lambda invoke --region us-east-1 --function-name $FUNCTION_NAME --payload '{ "queryStringParameters": { "ip": "1.2.3.4" } }  ' out --log-type Tail --query 'LogResult' --output text |  base64 -d |grep "Duration:" | awk '{print $5}')
  printf "${duration}ms\n"
  if [ "$i" != "1" ]; then
    duration_sum=`echo $duration_sum + $duration |bc`
  fi
done

average=`echo $duration_sum / 19 |bc`
echo ""
printf "Average Post Cold Start Duration: ${average}ms\n"
