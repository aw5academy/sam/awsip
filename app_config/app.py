import json
import urllib.request
import ipaddress
import os
import boto3

AWS_REGION = os.getenv('AWS_REGION')
APPCONFIG = boto3.client('appconfig', region_name=AWS_REGION)
APP_NAME = 'awsip'
ENV_NAME = 'live'

def lambda_handler(event, context):

  try:
    ip_address = event['queryStringParameters']['ip']
    print(ip_address)
    ipaddress.IPv4Address(ip_address)
  except:
    response = {
      "statusCode": 200,
      "body": json.dumps({"message": "Invalid ip!"})
    }
    response['headers'] = {"Access-Control-Allow-Origin": "*"}
    return response

  response_message = "false"
  for x in range(1, 3):
    response = APPCONFIG.get_configuration(
      Application=APP_NAME,
      Environment=ENV_NAME,
      Configuration=str(x),
      ClientId='lambda',
    )
    config = json.loads(response['Content'].read())
    for ip_network in config:
      if ipaddress.IPv4Address(ip_address) in ipaddress.IPv4Network(ip_network, False):
        response_message = "true"

  response = {
    "statusCode": 200,
    "body": json.dumps({"message": response_message})
  }
  response['headers'] = {"Access-Control-Allow-Origin": "*"}
  return response
