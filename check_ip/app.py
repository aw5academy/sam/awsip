import json
import urllib.request
import ipaddress

def lambda_handler(event, context):

  try:
    ip_address = event['queryStringParameters']['ip']
    print(ip_address)
    ipaddress.IPv4Address(ip_address)
  except:
    response = {
      "statusCode": 200,
      "body": json.dumps({"message": "Invalid ip!"})
    }
    response['headers'] = {"Access-Control-Allow-Origin": "*"}
    return response

  response_message = "false"
  for x in range(1, 3):
    url = f'http://localhost:2772/applications/awsip/environments/live/configurations/' + str(x)
    config = json.loads(urllib.request.urlopen(url).read())
    for ip_network in config:
      if ipaddress.IPv4Address(ip_address) in ipaddress.IPv4Network(ip_network, False):
        response_message = "true"

  response = {
    "statusCode": 200,
    "body": json.dumps({"message": response_message})
  }
  response['headers'] = {"Access-Control-Allow-Origin": "*"}
  return response
